# Linux Professional Institute Certification 201

## Welcome

### Please Choose Your Chanpter

-    [Detailed Description of this section](./Detailed.md)
-    [Capacity Planning](./200_Capacity_Planning/README.md)
-    [Linux Kernel](./201_Linux_Kernel/README.md)
-    [System Startup](./202_System_Startup/README.md)
-    [Filesystem and Devices](./203_FileSystem_and_Devices/README.md)
-    [Advanced Storage Device Administration](./204_Advanced_Storage_Device_Administration/README.md)
-    [Networking Configuration](./205_Network_Configuration/README.md)
-    [System Maintenance](./206_System_Maintenance/README.md)

