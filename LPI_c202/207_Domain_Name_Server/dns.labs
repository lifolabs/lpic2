Description

Creating a caching-only name server is the most basic type of BIND configuration. This configuration is the main building block for more advanced BIND configurations. Students will learn to configure a caching-only name server and test name resolution with the nslookup command.
Lab History
No attempts for this lab.
Categories
Linux
Learning Objectives
check_circle
Log in to the lab server with the `cloud_user` and issue `sudo -i` to gain root access.

$ ssh cloud_user@<"provided lab server IP">
Password:

$ sudo -i
[sudo] password for cloud_user: 
# 
# id
uid=0(root) gid=0(root) groups=0(root) context=unconfined_u:unconfined_r:unconfined_t:s0-s0:c0.c1023

check_circle
Install the `bind` and `bind-utils` packages.

# yum install -y bind bind-utils

check_circle
Verify the `named` configuration for `localhost`. Then check the configuration for syntax errors with the `named-checkconf` command.

# cat /etc/named.conf

# named-checkconf

check_circle
Start and enable the `named` service.

# systemctl start named
# systemctl enable named

check_circle
Run a DNS query against the DNS server defined in `/etc/resolv.conf`. Next, run a DNS query against `localhost`.

# nslookup google.com

# nslookup google.com localhost

---

Additional Information and Resources

First, we'll install the bind and bind-utils packages. Then, verify the named configuration for localhost and check the configuration with named-checkconf. Finally, we'll start the named service and test name resolution using the nslookup command.


#############

---


Description

BIND uses a shared secret key authentication method to grant privileges to hosts. It is important to know how to generate this key for administration purposes. In this hands-on lab, we will learn to configure the RNDC key and configuration file, and link it to the named service. To accomplish this, we will install the BIND package and recreate the RNDC key and configuration. We will then copy the new configuration to the named.conf file. To complete this lab, you will have to show that a new configuration has been created and that DNS queries are being cached on localhost.
Lab History
No attempts for this lab.
Categories
Linux
Learning Objectives
check_circle
Log in to the lab server with the `cloud_user` and issue `sudo -i` to gain root access.

$ ssh cloud_user@**.**.**.** . (your lab server IP)
Password:

$ sudo -i
[sudo] password for cloud_user: 
# 
# id
uid=0(root) gid=0(root) groups=0(root) context=unconfined_u:unconfined_r:unconfined_t:s0-s0:c0.c1023

check_circle
Install the `bind` and `bind-utils` packages. Start and enable the `named` service.

    Install bind and bind-utils:

    # yum install -y bind bind-utils

    Start and enable the named service:

    # systemctl start named
    # systemctl enable named

check_circle
Recreate the RNDC key and configuration file.

    Remove the rndc.key file.

    # rm /etc/rndc.key

    Stop the named service.

    # systemctl stop named

    Generate an rndc key and configuration file.

    # rndc-confgen -r /dev/urandom > /etc/rndc.conf

check_circle
Link the RNDC configuration to the `named` configuration.

    Open the /etc/rndc.conf file with vim:

    # vim /etc/rndc.conf

    Copy the section "Copy to the named.conf file"

    Open the /etc/named.conf file for editing with vim.

    # vim /etc/named.conf

    Paste the copied section into /etc/named.conf just before the include statements and delete the # signs at the beginning of the lines.

check_circle
Start the `named` service.

 # systemctl start named

check_circle
Test the configuration to ensure records are being cached on the localhost.

# nslookup www.google.com 127.0.0.1
---

Additional Information and Resources

The application team at ABC Company is experiencing an outage with their application. It was discovered that the key file for the named configuration was accidentally deleted. Since they were unable to restore the original file, we have been asked to recreate the RNDC key and link it to the named configuration.

    Use the yum utility to install bind and bind-utils.
    Remove the auto-generated /etc/rndc.key file in order to configure and link new keys.
    Create a new RNDC key and link it to the named configuration using rndc-confgen.
    Use systemctl to start the named service.
    Use the nslookup utility to verify that DNS records are being cached on localhost.

#######################

---


https://www.howtoforge.com/perfect-server-debian-10-buster-apache-bind-dovecot-ispconfig-3-1/


---

Description

Zones and zone files are key components in configuring DNS servers. In this lab, we will configure a forward zone and a forward zone file, then add TTL, SOA, NS, and A records. Next, we will run a syntax check on the named.conf and the forward zone file with named-checkconf and named-checkzone, respectively. This allows a name server to resolve a query, given the hostname, and returns the IP address. This is the most common type of DNS query.
Lab History
No attempts for this lab.
Categories
Linux
Learning Objectives
check_circle
Add the forward zone configuration to the `/etc/named.conf` file. Then run the `named-checkconf` command to verify the configuration.

    Add the forward zone configuration:

    vim /etc/named.conf

        Insert the zone configuration just before the include statements at the bottom of the file:

        zone "mylabserver.com" {
        type master;
        file "/var/named/fwd.mylabserver.com.db";
        };

        Run the named-checkconf command to verify the configuration.

        named-checkconf

check_circle
Create the forward zone file and check the configuration for syntax errors with `named-checkzone`.

    Create the forward zone file and enter the following information:

    vim /var/named/fwd.mylabserver.com.db

    Enter in the following:

    $TTL    86400
    @       IN      SOA     nameserver.mylabserver.com. root.mylabserver.com. (
                           10030         ; Serial
                            3600         ; Refresh
                            1800         ; Retry
                          604800         ; Expiry
                           86400         ; Minimum TTL
    )
    ; Name Server
    @        IN      NS       nameserver.mylabserver.com.
    ; A Record Definitions
    nameserver       IN      A       172.31.18.93
    mailprod         IN      A       172.31.18.30
    mailbackup       IN      A       172.31.18.72
    ; Canonical Name/Alias
    dns        IN    CNAME    nameserver.mylabserver.com.
    ; Mail Exchange Records
    @        IN    MX    10    mailprod.mylabserver.com.
    @        IN    MX    20    mailbackup.mylabserver.com.

    Run the named-checkzone command to check the zone file for syntax errors.

    named-checkzone mylabserver.com fwd.mylabserver.com.db

check_circle
Change the file permissions and the group owner for `/var/named/fwd.mylabserver.com.db`.

    Change the file permissions for /var/named/fwd.mylabserver.com.db:

    chmod 760 /var/named/fwd.mylabserver.com.db

    Change the group owner of the file to named.

    chgrp named /var/named/fwd.mylabserver.com.db

check_circle
Restart the named service.

systemctl restart named

check_circle
Run a query to test the configuration.

nslookup mailprod.mylabserver.com localhost

---

Additional Information and Resources

ABC Company is currently in the process of setting up its own internally-hosted DNS service. The next phase of the project is to create the forward zone files. The DNS administrator fell sick and is unavailable, but the project has a tight timeline. We have been designated as a resource to create the forward zone files until the DNS Administrator returns.

To complete this lab, we need to use the vim command to create and open a forward zone file and create the following records:

    TTL Record
    SOA Record
    A Records:
        nameserver
        mailprod
        mailbackup
    CNAME Record:
        dns = nameserver.mylabserver.com.

##########################

---

Description

Creating reverse zones is crucial to DNS server configuration, and in performing reverse name service lookups. In this video, we will see how to configure the zone in /etc/named.conf and create the Start of Authority record as well as other records in the reverse zone file. Then we'll test our configuration with a reverse query.
Lab History
No attempts for this lab.
Categories
Linux
Learning Objectives
check_circle
Get the IP Address for the Primary Interface for the Lab Server

Record the IP address for the primary interface:

ifconfig -a

check_circle
Add the Reverse Zone Configuration to the /etc/named.conf File. Then Run the named-checkconf Command to Verify the Configuration

vim /etc/named.conf

Take the last three octets of the public IP address and enter them in reverse where you see xx.xx.xxx (such as 11.31.172). Insert the zone configuration just before the include statements at the bottom of the file:

zone "xx.xx.xxx.in-addr.arpa" {
   type master;
   file "/var/named/xx.xx.xxx.db";
}

Then run the named-checkconf command to verify the configuration.

named-checkconf

check_circle
Create the Reverse Zone File and Check the Configuration for Syntax Errors with named-checkzone

    Create the reverse zone file and enter the following information:

     vim /var/named/xx.xx.xxx.db

    Enter in the following:

     $TTL    86400
     @       IN      SOA     nameserver.mylabserver.com. root.mylabserver.com. (
                               10030         ; Serial
                                3600         ; Refresh
                                1800         ; Retry
                              604800         ; Expiry
                               86400         ; Minimum TTL
     )
     ; Name Server
     @        IN      NS       nameserver.mylabserver.com.
     ; PTR Record Definitions
     93       IN      PTR       nameserver.mylabserver.com.
     30       IN      PTR       mailprod.mylabserver.com.
     72       IN      PTR       mailbackup.mylabserver.com.
     ; Mail Exchange Records
     @        IN    MX    10    mailprod.mylabserver.com.
     @        IN    MX    20    mailbackup.mylabserver.com.

    Run the named-checkzone command to check the zone file for syntax errors.

     named-checkzone mylabserver.com xx.xx.xxx.db

check_circle
Change the File Permissions and the Group Owner for /var/named/xx.xx.xxx.db

    Change the file permissions for /var/named/xx.xx.xxx.db:

     chmod 760 /var/named/xx.xx.xxx.db

    Change the group owner of the file to named:

     chgrp named /var/named/xx.xx.xxx.db

check_circle
Restart the named Service

systemctl restart named

check_circle
Run a Query to Test the Configuration

nslookup x.x.x.x localhost

Replace x.x.x.x with what ifconfig -a returned for an IP address earlier.

---

Additional Information and Resources

ABC Company is currently in the process of setting up its own internally-hosted DNS service. The next phase of the project is to create the reverse zones. The DNS administrator fell sick and is unavailable, but the project has a tight timeline. We have been designated as a resource to create the reverse zone and maintain files until the DNS Administrator returns.

To complete this lab, we'll need to use the vim command to add the zone configuration to the /etc/named.conf file. Then, create a reverse zone file. Also, create the following records in the reverse zone file:

    TTL Record
    SOA Record
    Name Server
    PTR Records:
        nameserver.mylabserver.com.
        mailprod.mylabserver.com.
        mailbackup.mylabserver.com.

#################################
---

Description

Name service queries are essential in retrieving information stored in DNS records. We will be using the tools in the bind-utils package to perform these requests. In this hands-on lab, we will perform name service queries using the nslookup, host, and dig commands.
Lab History
No attempts for this lab.
Categories
Linux
Learning Objectives
check_circle
Install the bind and bind-utils Packages with yum, Then Start and Enable the named Service

# yum install -y bind bind-utils

# systemctl start named

# systemctl enable named

check_circle
Use the host Command to Display the Name Servers for the Domain, google.com

# host -t ns google.com

check_circle
Use the host Command to Display the Mail Servers for google.com

# host -t mx google.com

check_circle
Resolve the IP Address for ns4.google.com Using the nslookup Command
check_circle
Use the nslookup Command to List the Name Servers for google.com

# nslookup -type=ns google.com

check_circle
Use the nslookup Command to List the Mail Servers Responsible for Mail Exchange for google.com

# nslookup -query=mx google.com

check_circle
Use the Debug Mode for nslookup to Provide more Details for google.com

# nslookup -debug google.com

check_circle
Use the dig Command to Resolve the IP for google.com

# dig google.com

check_circle
Use the dig Command to List the Name Servers for google.com

# dig ns google.com

check_circle
Using dig, List Only the 4 NS Records for google.com

# dig ns google.com +noall +answer

---
Additional Information and Resources

The DNS team at ABC Company has just rolled out a new DNS system. Part of this process was implementing three new caching name servers for the satellite offices outside of their main campus. The Technical Lead for Office XYZ has asked us to perform name service queries, to ensure new records are being returned to the offsite offices. He has also asked us to take this opportunity to train another staff member on name service query utilities.

To complete this lab, we must install the bind-utils package, then verify that queries resolve successfully using the nslookup, host, and dig commands.

#####################
---

Description

Isolating BIND in a chroot jail is common practice. It prevents any malicious user, who happens to gain access to the system by exploiting a BIND vulnerability, from further exploiting the system. In this lab, we'll practice setting up a jail for BIND.
Lab History
No attempts for this lab.
Categories
Linux
Learning Objectives
check_circle
Set up the chroot Jail for the BIND Service

In CentOS all we need to do is run yum install bind-chroot -y, and then ensure that the normal BIND service isn't set to run:

systemctl stop named
systemctl disable named
systemctl enable named-chroot

check_circle
Add the Forward Zone Configuration to the /etc/named.conf File, Then Run the named-checkconf Command to Verify the Configuration

 # vim /etc/named.conf

Insert the zone configuration just before the include statements at the bottom of the file:

 zone "mylabserver.com" {
    type master;
    file "/var/named/fwd.mylabserver.com.db";
 };

Then run the named-checkconf command to verify the configuration:

 # named-checkconf

check_circle
Create the Forward Zone File and Check the Configuration for Syntax Errors with named-checkzone

    Create the forward zone file:

     vim /var/named/fwd.mylabserver.com.db

    Enter the following:

     $TTL    86400
     @       IN      SOA     nameserver.mylabserver.com. root.mylabserver.com. (
                               10030         ; Serial
                                3600         ; Refresh
                                1800         ; Retry
                              604800         ; Expiry
                               86400         ; Minimum TTL
     )
     ; Name Server
     @        IN      NS       nameserver.mylabserver.com.
     ; A Record Definitions
     nameserver       IN      A       172.31.18.93
     mailprod         IN      A       172.31.18.30
     mailbackup       IN      A       172.31.18.72
     ; Canonical Name/Alias
     dns        IN    CNAME    nameserver.mylabserver.com.
     ; Mail Exchange Records
     @        IN    MX    10    mailprod.mylabserver.com.
     @        IN    MX    20    mailbackup.mylabserver.com.

    Save the document with :wq!.
    Run the named-checkzone command to check the zone file for syntax errors:

     named-checkzone mylabserver.com fwd.mylabserver.com.db

check_circle
Change the File Permissions and the Group Owner for /var/named/fwd.mylabserver.com.db

    Change the file permissions for /var/named/fwd.mylabserver.com.db:

     chmod 760 /var/named/fwd.mylabserver.com.db

    Change the group owner of the file to named:

     chgrp named /var/named/fwd.mylabserver.com.db

check_circle
Start the Newly Configured named-chroot Service

systemctl start named-chroot

---

Additional Information and Resources

ABC Company is currently in the process of setting up its own internally-hosted DNS service. The next phase of the project is to set BIND up in a chroot jail. The DNS administrator fell sick and is unavailable, but the project has a tight timeline. We have been designated as a resource to set up a BIND chroot jail with a forward DNS zone.

To complete this lab we'll need to set up a chroot jail and then create a forward zone file that contains the following records:

    TTL Record
    SOA Record
    A Records:
        nameserver
        mailprod
        mailbackup
    CNAME Record:
        dns = nameserver.mylabserver.com.

#######################
---

